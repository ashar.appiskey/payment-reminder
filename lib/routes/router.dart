import 'package:flutter/material.dart';
import 'package:paymentreminder/UI/add_payment_reminder.dart';
import 'package:paymentreminder/UI/edit_payment_reminder.dart';
import 'package:paymentreminder/UI/home_screen.dart';
import 'package:paymentreminder/UI/login.dart';

import 'package:paymentreminder/UI/signup.dart';

import 'index.dart';

class MyRouter {
  Widget getRouterWithScaleFactor(BuildContext context, Widget screenName,
      {RouteSettings? settings}) {
    return MediaQuery(
      data: MediaQuery.of(context).copyWith(textScaleFactor: 1),
      child: screenName,
    );
  }

  Route<dynamic> generateRoute(RouteSettings settings) {
    switch (settings.name) {
      case login:
        return MaterialPageRoute(
          builder: (context) =>
              getRouterWithScaleFactor(context, const Login()),
          settings: RouteSettings(arguments: settings.arguments),
        );

      case signUp:
        return MaterialPageRoute(
          builder: (context) =>
              getRouterWithScaleFactor(context, const SignUp()),
          settings: RouteSettings(arguments: settings.arguments),
        );
      case homeScreen:
        return MaterialPageRoute(
          builder: (context) =>
              getRouterWithScaleFactor(context, const HomeScreen()),
          settings: RouteSettings(arguments: settings.arguments),
        );
      case addPaymentReminder:
        return MaterialPageRoute(
          builder: (context) =>
              getRouterWithScaleFactor(context, const AddPaymentReminder()),
          settings: RouteSettings(arguments: settings.arguments),
        );

      case editPaymentReminder:
        return MaterialPageRoute(
          builder: (context) {
            final args = settings.arguments as Map;
            return getRouterWithScaleFactor(
                context, EditPaymentReminder(data: args));
          },
          settings: RouteSettings(
            arguments: settings.arguments,
          ),
        );

      default:
        return _errorRoute(settings.name);
    }
  }

  static Route<dynamic> _errorRoute(String? name) {
    return MaterialPageRoute(builder: (context) {
      return Scaffold(
        appBar: AppBar(
          title: const Text('ERROR'),
          centerTitle: true,
        ),
        body: Center(
          child: Text('No route defined for $name'),
        ),
      );
    });
  }
}
