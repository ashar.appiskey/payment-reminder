const String signUp = '/signUp';
const String login = '/login';
const String forgotPassword = '/forgotPassword';
const String homeScreen = '/homeScreen';
const String cameraScreen = '/cameraScreen';
const String conversationScreen = '/conversationScreen';
const String chatScreen = '/chatScreen';
const String accountScreen = '/accountScreen';
const String feedsScreen = '/feedsScreen';
const String postsScreen = '/postsScreen';
const String imageViewScreen = '/imageViewScreen';
const String addPaymentReminder = '/AddPaymentReminder';
const String editPaymentReminder = '/editPaymentReminder';
