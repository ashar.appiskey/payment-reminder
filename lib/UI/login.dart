import 'package:blurry_modal_progress_hud/blurry_modal_progress_hud.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:paymentreminder/constants/constants.dart';
import 'package:paymentreminder/providers/auth_provider.dart';
import 'package:paymentreminder/providers/toggle_password_field.dart';
import 'package:paymentreminder/routes/index.dart';
import 'package:paymentreminder/services/firebase_service.dart';
import 'package:paymentreminder/services/helper.dart';
import 'package:paymentreminder/services/shared_preferences.dart';
import 'package:paymentreminder/services/text_field_validators.dart';
import 'package:paymentreminder/widgets/buttons.dart';
import 'package:paymentreminder/widgets/custom_toast.dart';
import 'package:paymentreminder/widgets/input_field.dart';
import 'package:paymentreminder/widgets/text.dart';
import 'package:provider/provider.dart';

class Login extends StatefulWidget {
  const Login({super.key});

  @override
  State<Login> createState() => _LoginState();
}

class _LoginState extends State<Login> {
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  final FocusNode emailNode = FocusNode();
  final FocusNode passwordNode = FocusNode();
  bool _isEmailError = false;
  bool _isPasswordError = false;
  String _errorMessage = '';
  String fieldError = "Field is required";
  UserCredential? userCredential;
  User? user;

  @override
  void initState() {
    super.initState();

    CustomToast.instance.initialize(context);
    _emailController.addListener(() {
      if (_isEmailError && _emailController.text.trim().isNotEmpty) {
        setState(() {
          _isEmailError = false;
        });
      }
    });

    _passwordController.addListener(() {
      if (_isPasswordError && _passwordController.text.trim().isNotEmpty) {
        setState(() {
          _isPasswordError = false;
        });
      }
    });
  }

  @override
  void dispose() {
    _emailController.dispose();
    _passwordController.dispose();
    emailNode.dispose();
    passwordNode.dispose();
    super.dispose();
  }

  _onTapBackground() {
    print("adte");
    if (emailNode.hasFocus) {
      emailNode.unfocus();
    } else if (passwordNode.hasFocus) {
      passwordNode.unfocus();
    }
  }

  bool onPressLoginButton(BuildContext context) {
    //email validation
    if (_emailController.text.trim().isEmpty) {
      setState(() {
        _isEmailError = true;
        _errorMessage = fieldError;
      });

      return false;
    } else if (!Validator.instance
        .isEmailValidator(_emailController.text.trim())) {
      CustomToast.instance.showToast(context,
          message: "Invalid email format",
          fontSize: 1,
          isError: true,
          imagePath: R.image.error);
      return false;
    }
    //password validator
    else if (_passwordController.text.trim().isEmpty) {
      setState(() {
        _isPasswordError = true;
        _errorMessage = fieldError;
      });
      return false;
    }

    return true;
  }

  loginWithEmailPassword({
    required String email,
    required String password,
  }) async {
    Provider.of<AuthProvider>(context, listen: false).setLoading(true);

    try {
      userCredential =
          await FirebaseService.FAinstance.signInWithEmailAndPassword(
        email: email.trim().toString(),
        password: password.trim().toString(),
      ).then((value) async {
        SharedPreferenceManager.sharedInstance
            .storeToken(value.user!.uid.toString());
        await Provider.of<AuthProvider>(context, listen: false)
            .getProfile(value.user!.uid.toString());
        Provider.of<AuthProvider>(context, listen: false).setLoading(false);
        Navigator.of(context)
            .pushNamedAndRemoveUntil(homeScreen, (route) => false);
        return null;
      });
    } on FirebaseAuthException catch (e) {
      Provider.of<AuthProvider>(context, listen: false).setLoading(false);
      print(e.code);
      // if (e.code == 'weak-password') {
      Provider.of<AuthProvider>(context, listen: false).setLoading(false);
      CustomToast.instance.showToast(context,
          message: e.code.toString(),
          fontSize: 1,
          isError: true,
          imagePath: R.image.error);
      // print('The password provided is too weak.');
      // }
    } catch (e) {
      Provider.of<AuthProvider>(context, listen: false).setLoading(false);
      print(e);
    }
    return user;
  }

  @override
  Widget build(BuildContext context) {
    return BlurryModalProgressHUD(
      inAsyncCall: Provider.of<AuthProvider>(context).isLoading,
      blurEffectIntensity: 4,
      progressIndicator: CircularProgressIndicator(
        color: R.color.deepGreenColor,
      ),
      child: Scaffold(
        backgroundColor: R.color.whiteColor,
        body: WillPopScope(
          onWillPop: () async {
            // SystemNavigator.pop();
            return false;
          },
          child: GestureDetector(
            onTap: () => _onTapBackground(),
            child: SingleChildScrollView(
              physics: BouncingScrollPhysics(),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  SizedBox(
                    height: Helper.dynamicHeight(context, 10),
                  ),
                  Center(
                    child: Image.asset(
                      R.image.paymentImage,
                      width: Helper.dynamicWidth(context, 65),
                      height: Helper.dynamicWidth(context, 65),
                    ),
                  ),
                  SizedBox(
                    height: Helper.dynamicHeight(context, 3.5),
                  ),
                  TextWidget(
                    text: "Login",
                    fontSize: 3,
                  ),
                  SizedBox(
                    height: Helper.dynamicHeight(context, 3.5),
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(
                        horizontal: Helper.dynamicWidth(context, 5)),
                    child: TextFieldWithIcon(
                      node: emailNode,
                      controller: _emailController,
                      placeHolder: "Enter email",
                      errorText: _isEmailError ? _errorMessage : null,
                      isErrorBorder: _isEmailError,
                    ),
                  ),
                  SizedBox(
                    height: Helper.dynamicHeight(context, 2),
                  ),
                  Consumer<TogglePasswordVisibilty>(
                    builder: (context, passConsumer, child) {
                      return Padding(
                        padding: EdgeInsets.symmetric(
                            horizontal: Helper.dynamicWidth(context, 5)),
                        child: TextFieldWithIcon(
                          controller: _passwordController,
                          node: passwordNode,
                          placeHolder: "Password",
                          isSecure: passConsumer.isVisible,
                          isSuffixIcon: true,
                          onPressSuffix: () => passConsumer.updateVisibilty(),
                          imagePath: passConsumer.isVisible
                              ? R.image.hide
                              : R.image.visible,
                          errorText: _isPasswordError ? _errorMessage : null,
                          isErrorBorder: _isPasswordError,
                        ),
                      );
                    },
                  ),
                  SizedBox(
                    height: Helper.dynamicHeight(context, 6),
                  ),
                  ButtonWithGradientBackground(
                    text: "Login",
                    onPressed: () {
                      if (onPressLoginButton(context)) {
                        loginWithEmailPassword(
                            email: _emailController.text,
                            password: _passwordController.text);
                      }
                    },
                    fontSize: 1.6,
                  ),
                  SizedBox(
                    height: Helper.dynamicHeight(context, 4),
                  ),
                  GestureDetector(
                    onTap: () {
                      Navigator.of(context).pushNamed(signUp);
                    },
                    child: Center(
                      child: TextWidget(
                        text: "don't have an account? Register",
                        fontSize: 1,
                        color: R.color.blackColor,
                        fontFamily: R.fonts.montserratSemiBold,
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
