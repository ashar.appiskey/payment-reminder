import 'dart:async';

import 'package:blurry_modal_progress_hud/blurry_modal_progress_hud.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:paymentreminder/constants/constants.dart';
import 'package:paymentreminder/providers/auth_provider.dart';
import 'package:paymentreminder/providers/payments_provider.dart';
import 'package:paymentreminder/routes/index.dart';
import 'package:paymentreminder/services/firebase_service.dart';
import 'package:paymentreminder/services/helper.dart';
import 'package:paymentreminder/services/local_notification_service.dart';
import 'package:paymentreminder/services/shared_preferences.dart';
import 'package:paymentreminder/widgets/custom_appbar.dart';
import 'package:paymentreminder/widgets/payment_list_tile.dart';
import 'package:paymentreminder/widgets/show_permission_dialog.dart';
import 'package:paymentreminder/widgets/text.dart';
import 'package:provider/provider.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({super.key});

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  late final ScrollController _paymentListController;
  var _controller = StreamController<QuerySnapshot?>();
  List paymentIDs = [];

  @override
  void initState() {
    _paymentListController = ScrollController();
    super.initState();
    init();
    saveFirebaseToken();
  }

  saveFirebaseToken() async {
    String? fbToken = await FirebaseService.FMinstance.getToken();
    Provider.of<AuthProvider>(context, listen: false)
        .updateUserFireBaseToken(fbToken!);
    LocalNotificationService.initialize(context);
    firebaseNotification();
  }

  firebaseNotification() {
    ///gives you the message on which user taps
    ///and it opened the app from terminated state
    FirebaseService.FMinstance.getInitialMessage().then((message) {
      if (message != null) {
        print("getInitialMessage");
        LocalNotificationService.terminatedNotificationHandler(
            message, context);
      }
    });

    ///forground work
    FirebaseMessaging.onMessage.listen((message) async {
      if (message.notification != null) {
        print(message.notification!.body);
        print(message.notification!.title);
      }

      LocalNotificationService.display(message);
    });

    ///When the app is in background but opened and user taps
    ///on the notification
    FirebaseMessaging.onMessageOpenedApp.listen((message) async {
      print(message);
      print("onMessageOpenedApp");
      LocalNotificationService.backgroundNotificationHandler(message, context);
    });

    // FirebaseMessaging.onBackgroundMessage((message) async {
    //   // Handle the incoming message when the app is in the background.
    //   print(message);
    // });
  }

  // for getting specific user info
  Stream<QuerySnapshot<Map<String, dynamic>>> getUserInfo() {
    return FirebaseService.FFSinstance.collection("users")
        .where("userId",
            isEqualTo: SharedPreferenceManager.sharedInstance.getToken())
        .snapshots();
  }

  init() async {
    getUserInfo().forEach((element) {
      // print(element.docs[0]["myPamentsIds"]);
      // print(element.docs[0]["fullName"]);
      //get the paymentIDS from specific user to get the payment list of specific user
      if (element.docs[0]["myPamentsIds"].isNotEmpty) {
        FirebaseService.FFSinstance.collection("payment_reminders")
            .where("uid", whereIn: element.docs[0]["myPamentsIds"])
            .orderBy(
              'createdAt',
              descending: true,
            )
            .snapshots()
            .listen((data) {
          // print(data);
          if (_controller.isClosed) {
            _controller = StreamController<QuerySnapshot>();
            WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
              try {
                setState(() {
                  _controller.add(data);
                });
              } catch (e) {
                print(e);
              }
            });
          } else {
            _controller.add(data);
          }
        });
      } else {
        _controller.add(null);
        _controller.close();
      }
    });
  }

  @override
  void dispose() {
    _paymentListController.dispose();
    _controller.close();
    super.dispose();
  }

  Future<void> _signOut() async {
    //signout function
    Provider.of<PaymentsProvider>(context, listen: false).setLoading(true);
    await FirebaseService.FAinstance.signOut();
    //clear FB token
    await FirebaseService.FMinstance.deleteToken();
    await FirebaseService.FFSinstance.collection("users")
        .doc(SharedPreferenceManager.sharedInstance.getToken().toString())
        .update({
      'firebaseToken': "",
    });
    SharedPreferenceManager.sharedInstance.clearPref();
    Provider.of<PaymentsProvider>(context, listen: false).setLoading(false);
    Navigator.of(context).pushNamedAndRemoveUntil(login, (route) => false);
  }

  @override
  Widget build(BuildContext context) {
    return BlurryModalProgressHUD(
      inAsyncCall: Provider.of<PaymentsProvider>(context).isLoading,
      blurEffectIntensity: 4,
      progressIndicator: CircularProgressIndicator(
        color: R.color.deepGreenColor,
      ),
      child: Scaffold(
        backgroundColor: R.color.whiteColor,
        appBar: CustomAppBar(
          isLeadingIcon: false,
          isCenterTitle: true,
          title: "Payment Reminder",
          fontSize: 1.8,
          isTrailingIcon: true,
          trailingImage: R.image.logout,
          onTapTrailing: () => _signOut(),
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            Navigator.pushNamed(context, addPaymentReminder);
          },
          child: const Icon(Icons.add),
          backgroundColor: R.color.deepGreenColor,
        ),
        body: WillPopScope(
          onWillPop: () async {
            // SystemNavigator.pop();
            return false;
          },
          child: StreamBuilder(
            stream: _controller.stream,
            builder: (context, snapshot) {
              if (snapshot.connectionState == ConnectionState.waiting) {
                return Center(
                  child: CircularProgressIndicator(
                    color: R.color.deepGreenColor,
                  ),
                );
              } else if (snapshot.hasData) {
                return ListView.separated(
                  physics: const BouncingScrollPhysics(),
                  controller: _paymentListController,
                  itemCount: snapshot.data!.docs.length,
                  shrinkWrap: true,
                  itemBuilder: (context, index) {
                    // print(snapshot.data!.docs[index]["deleted_At"]);
                    return Visibility(
                      visible: snapshot.data!.docs[index]["deleted_At"] == null
                          ? true
                          : false,
                      child: PaymentListTile(
                        date: snapshot.data!.docs[index]["dueDate"],
                        description: snapshot.data!.docs[index]["description"],
                        title: snapshot.data!.docs[index]["title"],
                        isPaid: snapshot.data!.docs[index]["paid"],
                        onpressDelete: () {
                          ShowDialogBox.customDialogBoxDeleteUser(
                            context,
                            "Delete Payment",
                            "Are you sure you want to delete this?",
                            onPress: () async {
                              Navigator.of(context).pop();
                              await Provider.of<PaymentsProvider>(context,
                                      listen: false)
                                  .deletePaymentReminder(
                                      paymentId: snapshot.data!.docs[index]
                                          ["uid"]);
                            },
                            onCancel: () {
                              Navigator.of(context).pop();
                            },
                          );
                        },
                        onpressEdit: () {
                          Navigator.of(context).pushNamed(
                            editPaymentReminder,
                            arguments: {
                              'payment_Id': snapshot.data!.docs[index]["uid"],
                              'title': snapshot.data!.docs[index]["title"],
                              'description': snapshot.data!.docs[index]
                                  ["description"],
                              'dueDate': snapshot.data!.docs[index]["dueDate"],
                            },
                          );
                        },
                        onpressPaid: () {
                          ShowDialogBox.customDialogBoxDeleteUser(
                              context,
                              "Payment Done",
                              "Are you sure this payment has been done?",
                              onPress: () async {
                            Navigator.of(context).pop();
                            await Provider.of<PaymentsProvider>(context,
                                    listen: false)
                                .paidPaymentReminder(
                                    paymentId: snapshot.data!.docs[index]
                                        ["uid"]);
                          }, onCancel: () {
                            Navigator.of(context).pop();
                          });
                        },
                      ),
                    );
                  },
                  separatorBuilder: (context, index) {
                    return Divider(
                      height: Helper.dynamicHeight(context, 0.1),
                      indent: Helper.dynamicWidth(context, 6),
                      endIndent: Helper.dynamicWidth(context, 5),
                      thickness: 1,
                    );
                  },
                );
              } else {
                return Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Image.asset(
                        R.image.noPayment,
                        width: Helper.dynamicWidth(context, 40),
                        height: Helper.dynamicWidth(context, 40),
                      ),
                      TextWidget(
                        text: "No Payment Reminders",
                        fontFamily: R.fonts.montserratMedium,
                      ),
                    ],
                  ),
                );
              }
            },
          ),
        ),
      ),
    );
  }
}
