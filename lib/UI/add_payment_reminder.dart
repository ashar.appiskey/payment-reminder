import 'package:blurry_modal_progress_hud/blurry_modal_progress_hud.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:paymentreminder/constants/constants.dart';
import 'package:paymentreminder/providers/payments_provider.dart';
import 'package:paymentreminder/routes/index.dart';
import 'package:paymentreminder/services/helper.dart';
import 'package:paymentreminder/widgets/buttons.dart';
import 'package:paymentreminder/widgets/custom_appbar.dart';
import 'package:paymentreminder/widgets/input_field.dart';
import 'package:paymentreminder/widgets/text.dart';
import 'package:provider/provider.dart';

class AddPaymentReminder extends StatefulWidget {
  const AddPaymentReminder({super.key});

  @override
  State<AddPaymentReminder> createState() => _AddPaymentReminderState();
}

class _AddPaymentReminderState extends State<AddPaymentReminder> {
  final TextEditingController _titleController = TextEditingController();
  final TextEditingController _descriptionController = TextEditingController();
  final TextEditingController _dateController = TextEditingController();
  final FocusNode titleNode = FocusNode();
  final FocusNode descriptionNode = FocusNode();
  final FocusNode dateNode = FocusNode();
  bool _istitleError = false;
  bool _isDescriptionError = false;
  bool _isDateError = false;
  String _errorMessage = '';
  String fieldError = "Field is required";

  @override
  void initState() {
    super.initState();

    _titleController.addListener(() {
      if (_istitleError && _titleController.text.trim().isNotEmpty) {
        setState(() {
          _istitleError = false;
        });
      }
    });

    _descriptionController.addListener(() {
      if (_isDescriptionError &&
          _descriptionController.text.trim().isNotEmpty) {
        setState(() {
          _isDescriptionError = false;
        });
      }
    });

    _dateController.addListener(() {
      if (_isDateError && _dateController.text.trim().isNotEmpty) {
        setState(() {
          _isDescriptionError = false;
        });
      }
    });
  }

  @override
  void dispose() {
    _titleController.dispose();
    _descriptionController.dispose();
    _dateController.dispose();
    dateNode.dispose();
    titleNode.dispose();
    descriptionNode.dispose();

    super.dispose();
  }

  bool onPressDoneButton(BuildContext context) {
    //title validation
    if (_titleController.text.trim().isEmpty) {
      setState(() {
        _istitleError = true;
        _errorMessage = fieldError;
      });

      return false;
    }
    //password validator
    else if (_descriptionController.text.trim().isEmpty) {
      setState(() {
        _isDescriptionError = true;
        _errorMessage = fieldError;
      });
      return false;
    }

    //password validator
    else if (_dateController.text.trim().isEmpty) {
      setState(() {
        _isDateError = true;
        _errorMessage = fieldError;
      });
      return false;
    }

    return true;
  }

  datePicker() async {
    DateTime? pickedDate = await showDatePicker(
        context: context,
        initialDate: DateTime.now(), //get today's date
        firstDate: DateTime(
            2000), //DateTime.now() - not to allow to choose before today.
        lastDate: DateTime(2101));

    if (pickedDate != null) {
      //remove the date erorr
      setState(() {
        _isDateError = false;
        _errorMessage = fieldError;
      });
      print(
          pickedDate); //get the picked date in the format => 2022-07-04 00:00:00.000
      String formattedDate = DateFormat('yyyy-MM-dd').format(
          pickedDate); // format date in required form here we use yyyy-MM-dd that means time is removed
      print(
          formattedDate); //formatted date output using intl package =>  2022-07-04
      //You can format date as per your need

      setState(() {
        _dateController.text =
            formattedDate; //set foratted date to TextField value.
      });
    } else {
      print("Date is not selected");
    }
  }

  _onTapBackground() {
    if (dateNode.hasFocus) {
      dateNode.unfocus();
    } else if (titleNode.hasFocus) {
      titleNode.unfocus();
    } else if (descriptionNode.hasFocus) {
      descriptionNode.unfocus();
    }
  }

  @override
  Widget build(BuildContext context) {
    return BlurryModalProgressHUD(
      inAsyncCall: Provider.of<PaymentsProvider>(context).isLoading,
      blurEffectIntensity: 4,
      progressIndicator: CircularProgressIndicator(
        color: R.color.deepGreenColor,
      ),
      child: Scaffold(
        backgroundColor: R.color.whiteColor,
        appBar: CustomAppBar(
          leadImage: R.image.back,
          fontSize: 1.8,
          onTapLeading: () {
            Navigator.of(context).pop();
          },
        ),
        body: GestureDetector(
          onTap: () => _onTapBackground(),
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: EdgeInsets.symmetric(
                    horizontal: Helper.dynamicWidth(context, 3),
                  ),
                  child: TextWidget(
                    text: "Add new reminder",
                    fontSize: 1.5,
                    color: R.color.blackColor,
                    fontFamily: R.fonts.montserratMedium,
                  ),
                ),
                SizedBox(
                  height: Helper.dynamicHeight(context, 3),
                ),
                Container(
                  margin: EdgeInsets.symmetric(
                    horizontal: Helper.dynamicWidth(context, 3),
                  ),
                  height: Helper.dynamicHeight(context, 50),
                  decoration: BoxDecoration(
                    gradient: LinearGradient(
                      begin: Alignment.topCenter,
                      end: Alignment.bottomCenter,
                      colors: [
                        Color.fromRGBO(234, 234, 239, 1),
                        Color.fromRGBO(227, 225, 225, 1),
                      ],
                    ),
                    borderRadius: BorderRadius.circular(
                      Helper.dynamicFont(context, 0.5),
                    ),
                  ),
                  child: Column(
                    children: [
                      SizedBox(
                        height: Helper.dynamicHeight(context, 5),
                      ),
                      Padding(
                        padding: EdgeInsets.symmetric(
                          horizontal: Helper.dynamicWidth(context, 5),
                        ),
                        child: Align(
                          alignment: Alignment.topLeft,
                          child: TextWidget(
                            text: "Title",
                            fontSize: 1,
                            fontFamily: R.fonts.montserratMedium,
                            color: R.color.deepGreenColor,
                          ),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.symmetric(
                          horizontal: Helper.dynamicWidth(context, 5),
                        ),
                        child: SimpleTextField(
                          controller: _titleController,
                          node: titleNode,
                          placeHolder: "Add title",
                          maxLength: 15,
                          placeHolderColor: R.color.greyColor,
                          errorText: _istitleError ? _errorMessage : null,
                        ),
                      ),
                      SizedBox(
                        height: Helper.dynamicHeight(context, 5),
                      ),
                      //description field data
                      Padding(
                        padding: EdgeInsets.symmetric(
                          horizontal: Helper.dynamicWidth(context, 5),
                        ),
                        child: Align(
                          alignment: Alignment.topLeft,
                          child: TextWidget(
                            text: "Description",
                            fontSize: 1,
                            maxLines: 2,
                            fontFamily: R.fonts.montserratMedium,
                            color: R.color.deepGreenColor,
                          ),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.symmetric(
                          horizontal: Helper.dynamicWidth(context, 5),
                        ),
                        child: SimpleTextField(
                          controller: _descriptionController,
                          maxLength: 80,
                          node: descriptionNode,
                          placeHolder: "Add description",
                          placeHolderColor: R.color.greyColor,
                          errorText: _isDescriptionError ? _errorMessage : null,
                        ),
                      ),
                      //date picker
                      SizedBox(
                        height: Helper.dynamicHeight(context, 5),
                      ),

                      Padding(
                        padding: EdgeInsets.symmetric(
                          horizontal: Helper.dynamicWidth(context, 5),
                        ),
                        child: Align(
                          alignment: Alignment.topLeft,
                          child: TextWidget(
                            text: "Due date",
                            fontSize: 1,
                            maxLines: 2,
                            fontFamily: R.fonts.montserratMedium,
                            color: R.color.deepGreenColor,
                          ),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.symmetric(
                          horizontal: Helper.dynamicWidth(context, 5),
                        ),
                        child: SimpleTextField(
                          controller: _dateController,
                          node: dateNode,
                          readonly: true,
                          onTap: () => datePicker(),
                          placeHolder: "Add date",
                          maxLength: 10,
                          placeHolderColor: R.color.greyColor,
                          errorText: _isDateError ? _errorMessage : null,
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: Helper.dynamicHeight(context, 5),
                ),
                Center(
                  child: ButtonWithGradientBackground(
                    text: "Done",
                    onPressed: () async {
                      if (onPressDoneButton(context)) {
                        await Provider.of<PaymentsProvider>(context,
                                listen: false)
                            .createPaymentReminder(
                                _titleController.text,
                                _descriptionController.text,
                                _dateController.text);
                        Navigator.of(context).pushNamedAndRemoveUntil(
                            homeScreen, (route) => false);
                      }
                    },
                    fontSize: 1.6,
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
