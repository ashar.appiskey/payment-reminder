import 'package:blurry_modal_progress_hud/blurry_modal_progress_hud.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:paymentreminder/constants/constants.dart';
import 'package:paymentreminder/providers/auth_provider.dart';
import 'package:paymentreminder/providers/toggle_password_field.dart';

import 'package:paymentreminder/routes/index.dart';
import 'package:paymentreminder/services/firebase_service.dart';
import 'package:paymentreminder/services/helper.dart';
import 'package:paymentreminder/services/text_field_validators.dart';
import 'package:paymentreminder/widgets/buttons.dart';
import 'package:paymentreminder/widgets/custom_appbar.dart';
import 'package:paymentreminder/widgets/custom_toast.dart';
import 'package:paymentreminder/widgets/input_field.dart';
import 'package:paymentreminder/widgets/text.dart';
import 'package:provider/provider.dart';

class SignUp extends StatefulWidget {
  const SignUp({super.key});

  @override
  State<SignUp> createState() => _SignUpState();
}

class _SignUpState extends State<SignUp> {
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  final TextEditingController _fullNameController = TextEditingController();
  final TextEditingController _addressController = TextEditingController();

  final FocusNode emailNode = FocusNode();
  final FocusNode passwordNode = FocusNode();
  final FocusNode fullNameNode = FocusNode();
  final FocusNode addressNode = FocusNode();

  bool _isEmailError = false;
  bool _isPasswordError = false;
  bool _isFullNameError = false;

  bool _isAgeError = false;
  String fieldError = "Field is required";
  String _errorMessage = '';

  @override
  void initState() {
    super.initState();
    CustomToast.instance.initialize(context);
    _emailController.addListener(() {
      if (_isEmailError && _emailController.text.trim().isNotEmpty) {
        setState(() {
          _isEmailError = false;
        });
      }
    });

    _passwordController.addListener(() {
      if (_isPasswordError && _passwordController.text.trim().isNotEmpty) {
        setState(() {
          _isPasswordError = false;
        });
      }
    });

    _fullNameController.addListener(() {
      if (_isFullNameError && _fullNameController.text.trim().isNotEmpty) {
        setState(() {
          _isFullNameError = false;
        });
      }
    });

    _addressController.addListener(() {
      if (_isAgeError && _addressController.text.trim().isNotEmpty) {
        setState(() {
          _isAgeError = false;
        });
      }
    });
  }

  @override
  void dispose() {
    _emailController.dispose();
    _passwordController.dispose();
    _fullNameController.dispose();
    _addressController.dispose();

    emailNode.dispose();
    passwordNode.dispose();
    fullNameNode.dispose();
    addressNode.dispose();

    super.dispose();
  }

  _onTapBackground() {
    if (emailNode.hasFocus) {
      emailNode.unfocus();
    } else if (passwordNode.hasFocus) {
      passwordNode.unfocus();
    } else if (addressNode.hasFocus) {
      addressNode.unfocus();
    } else if (fullNameNode.hasFocus) {
      fullNameNode.unfocus();
    }
  }

  bool _onPressSignUpButton(BuildContext context) {
    //email validation
    if (_emailController.text.trim().isEmpty) {
      setState(() {
        _isEmailError = true;
        _errorMessage = fieldError;
      });
      return false;
    } else if (!Validator.instance
        .isEmailValidator(_emailController.text.trim())) {
      CustomToast.instance.showToast(context,
          message: "Invalid email format",
          fontSize: 1,
          isError: true,
          imagePath: R.image.error);
      return false;
    }
    //password validator
    else if (_passwordController.text.trim().isEmpty) {
      setState(() {
        _isPasswordError = true;
        _errorMessage = fieldError;
      });
      return false;
    }

    //fullname validator
    else if (_fullNameController.text.trim().isEmpty) {
      setState(() {
        _isFullNameError = true;
        _errorMessage = fieldError;
      });
      return false;
    }

    return true;
  }

  registerUsingEmailPassword({
    required String email,
    required String password,
  }) async {
    Provider.of<AuthProvider>(context, listen: false).setLoading(true);
    User? user;
    try {
      UserCredential userCredential =
          await FirebaseService.FAinstance.createUserWithEmailAndPassword(
        email: email.trim().toString(),
        password: password.trim().toString(),
      );
      user = userCredential.user;
      user = FirebaseService.FAinstance.currentUser;
      print(userCredential.user!.uid);

      await Provider.of<AuthProvider>(context, listen: false)
          .createUser(user!.uid, _fullNameController.text, user.email!,
              _addressController.text)
          .then((value) {
        Provider.of<AuthProvider>(context, listen: false).setLoading(false);
        Navigator.of(context)
            .pushNamedAndRemoveUntil(homeScreen, (route) => false);
      });
    } on FirebaseAuthException catch (e) {
      if (e.code == 'weak-password') {
        Provider.of<AuthProvider>(context, listen: false).setLoading(false);
        CustomToast.instance.showToast(context,
            message: "The password provided is too weak.",
            fontSize: 1,
            isError: true,
            imagePath: R.image.error);
        print('The password provided is too weak.');
      } else if (e.code == 'email-already-in-use') {
        Provider.of<AuthProvider>(context, listen: false).setLoading(false);
        CustomToast.instance.showToast(context,
            message: "The account already exists for that email.",
            fontSize: 1,
            isError: true,
            imagePath: R.image.error);
        print('The account already exists for that email.');
      }
    } catch (e) {
      Provider.of<AuthProvider>(context, listen: false).setLoading(false);
      print(e);
    }
    return user;
  }

  @override
  Widget build(BuildContext context) {
    return BlurryModalProgressHUD(
      inAsyncCall: Provider.of<AuthProvider>(context).isLoading,
      blurEffectIntensity: 4,
      progressIndicator: CircularProgressIndicator(
        color: R.color.deepGreenColor,
      ),
      child: Scaffold(
        backgroundColor: R.color.whiteColor,
        appBar: CustomAppBar(
          leadImage: R.image.back,
          onTapLeading: () {
            Navigator.of(context).pop();
          },
        ),
        body: GestureDetector(
          onTap: () => _onTapBackground(),
          child: SingleChildScrollView(
            physics: BouncingScrollPhysics(),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                SizedBox(
                  height: Helper.dynamicHeight(context, 1),
                ),
                Center(
                  child: Image.asset(
                    R.image.paymentImage,
                    width: Helper.dynamicWidth(context, 40),
                    height: Helper.dynamicWidth(context, 40),
                  ),
                ),
                SizedBox(
                  height: Helper.dynamicHeight(context, 1),
                ),
                TextWidget(
                  text: "Signup",
                  fontSize: 3,
                ),
                SizedBox(
                  height: Helper.dynamicHeight(context, 2),
                ),
                Padding(
                  padding: EdgeInsets.symmetric(
                      horizontal: Helper.dynamicWidth(context, 5)),
                  child: TextFieldWithIcon(
                    controller: _emailController,
                    node: emailNode,
                    placeHolder: "Email",
                    errorText: _isEmailError ? _errorMessage : null,
                    isErrorBorder: _isEmailError,
                  ),
                ),
                SizedBox(
                  height: Helper.dynamicHeight(context, 2),
                ),
                Consumer<TogglePasswordVisibilty>(
                  builder: (context, passConsumer, child) {
                    return Padding(
                      padding: EdgeInsets.symmetric(
                          horizontal: Helper.dynamicWidth(context, 5)),
                      child: TextFieldWithIcon(
                        controller: _passwordController,
                        node: passwordNode,
                        placeHolder: "Password",
                        isSecure: passConsumer.isVisible,
                        isSuffixIcon: true,
                        onPressSuffix: () => passConsumer.updateVisibilty(),
                        imagePath: passConsumer.isVisible
                            ? R.image.hide
                            : R.image.visible,
                        errorText: _isPasswordError ? _errorMessage : null,
                        isErrorBorder: _isPasswordError,
                      ),
                    );
                  },
                ),
                SizedBox(
                  height: Helper.dynamicHeight(context, 2),
                ),
                Padding(
                  padding: EdgeInsets.symmetric(
                      horizontal: Helper.dynamicWidth(context, 5)),
                  child: TextFieldWithIcon(
                    controller: _fullNameController,
                    node: fullNameNode,
                    placeHolder: "Full Name",
                    errorText: _isFullNameError ? _errorMessage : null,
                    isErrorBorder: _isFullNameError,
                  ),
                ),
                SizedBox(
                  height: Helper.dynamicHeight(context, 2),
                ),
                Padding(
                  padding: EdgeInsets.symmetric(
                      horizontal: Helper.dynamicWidth(context, 5)),
                  child: TextFieldWithIcon(
                    controller: _addressController,
                    node: addressNode,
                    placeHolder: "Address",
                    errorText: _isAgeError ? _errorMessage : null,
                    isErrorBorder: _isAgeError,
                  ),
                ),
                SizedBox(
                  height: Helper.dynamicHeight(context, 2),
                ),
                ButtonWithGradientBackground(
                  text: "Signup",
                  onPressed: () {
                    if (_onPressSignUpButton(context)) {
                      _onTapBackground();
                      registerUsingEmailPassword(
                          email: _emailController.text,
                          password: _passwordController.text);
                    }
                  },
                  fontSize: 1.6,
                ),
                SizedBox(
                  height: Helper.dynamicHeight(context, 4),
                ),
                GestureDetector(
                  onTap: () {
                    Navigator.of(context).pushNamed(login);
                  },
                  child: Center(
                    child: TextWidget(
                      text: "have an account? Signin",
                      fontSize: 1,
                      color: R.color.blackColor,
                      fontFamily: R.fonts.montserratSemiBold,
                    ),
                  ),
                ),
                SizedBox(
                  height: Helper.dynamicHeight(context, 4),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
