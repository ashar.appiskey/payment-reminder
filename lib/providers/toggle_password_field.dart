import 'package:flutter/material.dart';

class TogglePasswordVisibilty extends ChangeNotifier {
  bool isVisible = true;

  updateVisibilty() {
    isVisible = !isVisible;
    notifyListeners();
  }
}
