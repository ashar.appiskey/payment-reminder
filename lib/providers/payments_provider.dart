import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:paymentreminder/services/firebase_service.dart';
import 'package:paymentreminder/services/shared_preferences.dart';
import 'package:paymentreminder/services/uuid_service.dart';

class PaymentsProvider extends ChangeNotifier {
  bool isLoading = false;

  setLoading(bool value) {
    //set loading
    isLoading = value;
    notifyListeners();
  }

  //This method is used to createPaymentReminder in firestore
  Future<void> createPaymentReminder(
      String title, String description, String dueDate) async {
    setLoading(true);

    // uuid as a primary key
    var uUID = UUID.uuidInstance.uuid();
    //add payment reminder to firestore
    await FirebaseService.FFSinstance.collection("payment_reminders")
        .doc(uUID.toString())
        .set({
      'uid': uUID.toString(),
      'userId': SharedPreferenceManager.sharedInstance.getToken().toString(),
      'title': title.trim(),
      'description': description.trim(),
      'dueDate': dueDate,
      'notification_fire_time': null,
      'deleted_At': null,
      'paid': false,
      'createdAt': FieldValue.serverTimestamp()
    });

    //add the paymentreminder ID in users table to filter them in payments list while fetching them

    await FirebaseService.FFSinstance.collection('users')
        .doc(SharedPreferenceManager.sharedInstance.getToken())
        .update({
      'myPamentsIds': FieldValue.arrayUnion([uUID.toString()])
    });

    setLoading(false);
  }

  //This method is used to edit paymentRemiders in firestore
  Future<void> editPaymentReminder(
      {required String title,
      required String description,
      required String dueDate,
      required String paymentId}) async {
    setLoading(true);

    //edit payment reminder to firestore
    await FirebaseService.FFSinstance.collection("payment_reminders")
        .doc(paymentId)
        .update({
      'title': title.trim(),
      'description': description.trim(),
      'dueDate': dueDate,
      'createdAt': FieldValue.serverTimestamp()
    });

    setLoading(false);
  }

  //This method is used to edit paymentRemiders in firestore
  Future<void> deletePaymentReminder({required String paymentId}) async {
    setLoading(true);

    //edit payment reminder to firestore
    await FirebaseService.FFSinstance.collection("payment_reminders")
        .doc(paymentId)
        .update(
      {'deleted_At': FieldValue.serverTimestamp()},
    );

    setLoading(false);
  }

  //This method is used to paif payment in firestore
  Future<void> paidPaymentReminder({required String paymentId}) async {
    setLoading(true);

    //edit payment reminder to firestore
    await FirebaseService.FFSinstance.collection("payment_reminders")
        .doc(paymentId)
        .update(
      {'paid': true},
    );

    setLoading(false);
  }
}
