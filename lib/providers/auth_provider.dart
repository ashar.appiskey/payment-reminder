import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:paymentreminder/models/user_model.dart';
import 'package:paymentreminder/services/firebase_service.dart';
import 'package:paymentreminder/services/shared_preferences.dart';

class AuthProvider extends ChangeNotifier {
  bool isLoading = false;

  //This method is used to create the user in firestore
  Future<void> createUser(
    String uid,
    String fullName,
    String email,
    String? address,
  ) async {
    await FirebaseService.FFSinstance.collection("users")
        .doc(uid.toString())
        .set({
      'userId': uid.trim(),
      'fullName': fullName.trim(),
      'email': email.trim(),
      'address': address ?? "",
      'firebaseToken': "",
      'myPamentsIds': [],
      'createdAt': FieldValue.serverTimestamp()
    });

    getProfile(uid.trim());
  }

  Future getProfile(String token) async {
    // get the user profile and store it in sharedpreff
    QuerySnapshot<Map<String, dynamic>> snapshot =
        await FirebaseService.FFSinstance.collection("users")
            .where("userId", isEqualTo: token)
            .get();
    var profile = snapshot.docs
        .map((docSnapshot) => UserModel.fromDocumentJson(docSnapshot))
        .toList();
    storeProfile(profile.first);
  }

  storeProfile(UserModel profile) {
    SharedPreferenceManager.sharedInstance.storeToken(profile.userId);
    SharedPreferenceManager.sharedInstance.clearKey("profile");
    SharedPreferenceManager.sharedInstance.storeProfile(
      UserModel.fromJson(
        {
          'userId': profile.userId,
          'fullName': profile.fullName,
          'email': profile.email,
          'address': profile.address,
          'firebaseToken': profile.firebaseToken,
          'myPamentsIds':profile.myPamentsIds
        },
      ),
    );
  }

  Future<void> updateUserFireBaseToken(String fbToken) async {
    await FirebaseService.FFSinstance.collection("users")
        .doc(SharedPreferenceManager.sharedInstance.getToken().toString())
        .update({"firebaseToken": fbToken.toString()});

    getProfile(SharedPreferenceManager.sharedInstance.getToken().toString());
  }

  setLoading(bool value) {
    //set loading
    isLoading = value;
    notifyListeners();
  }
}
