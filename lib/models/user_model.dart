// To parse this JSON data, do
//
//     final userModel = userModelFromJson(jsonString);

import 'dart:convert';

import 'package:cloud_firestore/cloud_firestore.dart';

UserModel userModelFromJson(String str) => UserModel.fromJson(json.decode(str));

String userModelToJson(UserModel data) => json.encode(data.toJson());

class UserModel {
  UserModel({
    required this.userId,
    required this.fullName,
    required this.email,
    required this.address,
    required this.firebaseToken,
    required this.myPamentsIds,
  });

  String userId;
  String fullName;
  String email;
  String address;
  String firebaseToken;
  List myPamentsIds;

  factory UserModel.fromJson(Map<String, dynamic> json) => UserModel(
      userId: json["userId"],
      fullName: json["fullName"],
      email: json["email"],
      address: json["address"],
      firebaseToken: json["firebaseToken"],
      myPamentsIds: json["myPamentsIds"]);
  factory UserModel.fromDocumentJson(
          DocumentSnapshot<Map<String, dynamic>> json) =>
      UserModel(
        userId: json["userId"],
        fullName: json["fullName"],
        email: json["email"],
        address: json["address"],
        firebaseToken: json["firebaseToken"],
        myPamentsIds: ["myPamentsIds"],
      );
  Map<String, dynamic> toJson() => {
        "userId": userId,
        "fullName": fullName,
        "email": email,
        "address": address,
        "firebaseToken": firebaseToken,
        "myPamentsIds": myPamentsIds,
      };
}
