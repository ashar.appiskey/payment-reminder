import 'dart:ui';

class R {
  static final image = _Images();
  static final color = _Color();
  static final fonts = _Fonts();
}

class _Images {
  final back = 'assets/images/back.png';
  final visible = 'assets/images/visible.png';
  final hide = 'assets/images/Hide.png';
  final delete = 'assets/images/delete.png';
  final dropdown = 'assets/images/dropdown.png';
  final paymentImage = 'assets/images/payment image.jpg';
  final error = 'assets/images/error.png';
  final logout = 'assets/images/logout.png';
   final noPayment = 'assets/images/nopayment.jpg';
  
}

class _Fonts {
  final montserratBold = "MontserratBold";
  final montserratSemiBold = "MontserratSemiBold";
  final montserratRegular = "MontserratRegular";
  final montserratMedium = "MontserratMedium";
}

class _Color {
  final deepGreenColor = const Color(0xFF20B688);
  final deeplowGreenColor = const Color(0xFF3FE480);
  final redColor = const Color(0xFFFF6C53);
  final blueColor = const Color(0xFF84CBFE);
  final lowGreenColor = const Color(0xFF8BD1B9);
  final highGreenColor = const Color(0xFF9FF5C2);
  final blackColor = const Color(0xFF000000);
  final whiteColor = const Color(0xFFFFFFFF);
  final greyColor = const Color(0xFFA7A7A7);
  final midGreyColor = const Color(0xFFD9D9D9);
  final lightGreyColor = const Color(0xFFF2F2F2);
}
