import 'dart:io';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:paymentreminder/constants/constants.dart';
import 'package:paymentreminder/services/helper.dart';
import 'package:paymentreminder/widgets/buttons.dart';
import 'package:paymentreminder/widgets/text.dart';

class ShowDialogBox {
  static void dialogBox(context, title, content,
      {required Function onPress, required Function onCancel}) {
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return Platform.isIOS
            ? CupertinoAlertDialog(
                title: Text(title),
                content: Text(content),
                actions: [
                  CupertinoDialogAction(
                    child: const Text("No",
                        style: TextStyle(
                          color: Colors.red,
                        )),
                    onPressed: () {
                      Navigator.of(context).pop();
                      onCancel();
                    },
                  ),
                  CupertinoDialogAction(
                    child: const Text(
                      "Yes",
                    ),
                    onPressed: () {
                      onPress();
                      Navigator.of(context).pop();
                    },
                  ),
                ],
              )
            : AlertDialog(
                title: Text(title),
                content: Text(content),
                actions: [
                  TextButton(
                    onPressed: () {
                      Navigator.pop(context);
                      onCancel();
                    },
                    child: const Text('No',
                        style: TextStyle(
                          color: Colors.red,
                        )),
                  ),
                  TextButton(
                    onPressed: () {
                      onPress();
                      Navigator.of(context).pop();
                    },
                    child: const Text('Yes'),
                  ),
                ],
              );
      },
    );
  }

  static customDialogBoxDeleteUser(
    context,
    title,
    point1, {
    required Function onPress,
    required Function onCancel,
    double boxMinHeight = 32,
    double boxMaxHeight = 32,
    double boxWidth = 100,
    double boxSymetricPadding = 10,
    bool isDismissAble = false,
    MainAxisAlignment mainAxisAlignment = MainAxisAlignment.center,
  }) {
    showDialog(
      context: context,
      barrierColor: Colors.black38.withOpacity(0.8),
      builder: (BuildContext context) {
        return AlertDialog(
          backgroundColor: Colors.transparent,
          contentPadding: EdgeInsets.zero,
          content: Container(
            width: Helper.dynamicWidth(context, boxWidth),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(20),
              color: R.color.lightGreyColor,
            ),
            child: Column(
              // crossAxisAlignment: CrossAxisAlignment.end,
              mainAxisAlignment: mainAxisAlignment,
              mainAxisSize: MainAxisSize.min,
              children: [
                SizedBox(
                  height: Helper.dynamicHeight(context, 3),
                ),
                TextWidget(
                  textAlign: TextAlign.center,
                  text: title,
                  color: R.color.blackColor,
                  fontSize: 1.5,
                ),
                SizedBox(
                  height: Helper.dynamicHeight(context, 3),
                ),
                Padding(
                  padding: EdgeInsets.symmetric(
                    horizontal:
                        Helper.dynamicWidth(context, boxSymetricPadding),
                  ),
                  child: TextWidget(
                    textAlign: TextAlign.center,
                    text: point1,
                    color: R.color.blackColor,
                    fontSize: 1.3,
                    fontFamily: R.fonts.montserratRegular,
                  ),
                ),
                SizedBox(
                  height: Helper.dynamicHeight(context, 2.5),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    ButtonWithGradientBackground(
                      text: "Confirm",
                      onPressed: onPress,
                      buttonWidth: 30,
                      buttonHeight: 5,
                      fontSize: 1.5,
                      radius: 30,
                      linearGradient: const LinearGradient(
                        colors: [
                          Color.fromRGBO(248, 45, 75, 1),
                          Color.fromRGBO(255, 104, 127, 1),
                        ],
                        begin: Alignment.centerLeft,
                        end: Alignment.centerRight,
                      ),
                    ),
                    SizedBox(
                      width: Helper.dynamicWidth(context, 5),
                    ),
                    ButtonWithGradientBackground(
                      text: "Cancel",
                      onPressed: onCancel,
                      buttonWidth: 30,
                      buttonHeight: 5,
                      fontSize: 1.5,
                      radius: 30,
                    ),
                  ],
                ),
                SizedBox(
                  height: Helper.dynamicHeight(context, 5),
                ),
              ],
            ),
          ),
        );
      },
    );
  }

  static customDialogBox(
    context,
    title, {
    required Function onPress,
    required Function onCancel,
    double boxMinHeight = 32,
    double boxMaxHeight = 32,
    double boxWidth = 100,
    double boxSymetricPadding = 2,
    bool isDismissAble = false,
    MainAxisAlignment mainAxisAlignment = MainAxisAlignment.center,
  }) {
    showDialog(
      context: context,
      barrierColor: Colors.black38.withOpacity(0.8),
      builder: (BuildContext context) {
        return AlertDialog(
          backgroundColor: Colors.transparent,
          contentPadding: EdgeInsets.zero,
          content: Container(
            constraints: BoxConstraints(
              minHeight: Helper.dynamicHeight(context, boxMinHeight),
              maxHeight: Helper.dynamicHeight(context, boxMaxHeight),
            ),
            width: Helper.dynamicWidth(context, boxWidth),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(20),
              color: const Color.fromRGBO(254, 245, 245, 1),
            ),
            child: Center(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                mainAxisAlignment: mainAxisAlignment,
                children: [
                  Visibility(
                    visible: isDismissAble,
                    child: IconButtonWithGradientBackground(
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                      linearGradient: const LinearGradient(
                          colors: [Colors.transparent, Colors.transparent]),
                      icon: Icons.close,
                      iconColor:
                          (Theme.of(context).textTheme.subtitle2!.color)!,
                    ),
                  ),
                  Visibility(
                    child: SizedBox(
                      height: Helper.dynamicHeight(context, 4),
                    ),
                    visible: isDismissAble,
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(
                      horizontal:
                          Helper.dynamicWidth(context, boxSymetricPadding),
                    ),
                    child: Center(
                      child: TextWidget(
                        textAlign: TextAlign.center,
                        text: title,
                        color: Theme.of(context).textTheme.subtitle2!.color,
                        fontSize: 1.6,
                        fontFamily: "HalveticaNeueMedium",
                      ),
                    ),
                  ),
                  SizedBox(
                    height: Helper.dynamicHeight(context, 3),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      ButtonWithGradientBackground(
                        text: "Yes",
                        onPressed: onPress,
                        buttonWidth: 30,
                        fontSize: 1.5,
                        radius: 30,
                        linearGradient: const LinearGradient(
                          colors: [
                            Color.fromRGBO(15, 150, 150, 1),
                            Color.fromRGBO(33, 220, 219, 1),
                          ],
                          begin: Alignment.centerLeft,
                          end: Alignment.centerRight,
                        ),
                      ),
                      SizedBox(
                        width: Helper.dynamicWidth(context, 5),
                      ),
                      ButtonWithGradientBackground(
                        text: "No",
                        onPressed: onCancel,
                        buttonWidth: 30,
                        fontSize: 1.5,
                        radius: 30,
                        linearGradient: const LinearGradient(
                          colors: [
                            Color.fromRGBO(248, 45, 75, 1),
                            Color.fromRGBO(255, 104, 127, 1),
                          ],
                          begin: Alignment.centerLeft,
                          end: Alignment.centerRight,
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
        );
      },
    );
  }
}
