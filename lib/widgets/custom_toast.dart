import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:paymentreminder/services/helper.dart';
import 'package:paymentreminder/widgets/text.dart';

class CustomToast {
  String? _message;
  Color? _color;

  CustomToast._();
  static final CustomToast _singleton = CustomToast._();

  static CustomToast get instance => _singleton;

  FToast fToast = FToast();
  void initialize(BuildContext context) {
    print('Initialized Custom Toast');
    fToast.init(context);
  }

  void showToast(
    context, {
    Color backgroundColor = Colors.white,
    required String message,
    required String imagePath,
    Color shadowcolor = const Color.fromRGBO(172, 213, 212, 0.7),
    double fontSize = 2,
    Color messageColor = Colors.black,
    ToastGravity gravity = ToastGravity.BOTTOM,
    MainAxisSize mainAxisSize = MainAxisSize.min,
    TextAlign textAlign = TextAlign.start,
    bool isError = false,
  }) {
    fToast.showToast(
      child: Container(
        padding: EdgeInsets.symmetric(
          vertical: Helper.dynamicHeight(context, 0.2),
          horizontal: Helper.dynamicHeight(context, 0.2),
        ),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10.0),
          boxShadow: [
            BoxShadow(
              color: isError
                  ? const Color.fromRGBO(255, 160, 171, 0.5)
                  : shadowcolor,
              spreadRadius: 1.5,
              blurRadius: 6,
            ),
          ],
          color: backgroundColor,
        ),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisSize: mainAxisSize,
          children: [
            SizedBox(
              width: Helper.dynamicWidth(context, 5),
            ),
            SizedBox(
              height: Helper.dynamicHeight(context, 6),
              width: Helper.dynamicWidth(context, 6),
              child: Image.asset(imagePath),
            ),
            SizedBox(
              width: Helper.dynamicWidth(context, 2),
            ),
            Expanded(
              child: Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(6.0),
                  color: backgroundColor,
                ),
                child: TextWidget(
                  text: message,
                  fontSize: fontSize,
                  color: Theme.of(context).textTheme.subtitle2!.color,
                  textAlign: textAlign,
                ),
              ),
            ),
            SizedBox(
              width: Helper.dynamicWidth(context, 5),
            ),
          ],
        ),
      ),
      gravity: gravity,
      toastDuration: const Duration(seconds: 2),
    );
  }
}
