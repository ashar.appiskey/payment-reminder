import 'package:flutter/material.dart';
import 'package:paymentreminder/constants/constants.dart';
import 'package:paymentreminder/services/helper.dart';
import 'package:paymentreminder/widgets/text.dart';

import 'buttons.dart';

class PaymentListTile extends StatelessWidget {
  final String title;
  final String description;
  final String date;
  final VoidCallback onpressDelete;
  final VoidCallback onpressEdit;
  final VoidCallback onpressPaid;
  final bool isPaid;
  const PaymentListTile({
    super.key,
    required this.title,
    required this.description,
    required this.date,
    required this.onpressDelete,
    required this.onpressEdit,
    required this.onpressPaid,
    required this.isPaid,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      height: Helper.dynamicHeight(context, 21),
      width: Helper.dynamicWidth(context, 100),
      margin: EdgeInsets.all(
        Helper.dynamicFont(context, 1),
      ),
      padding: EdgeInsets.symmetric(
        vertical: Helper.dynamicHeight(context, 2),
        horizontal: Helper.dynamicWidth(context, 2),
      ),
      decoration: BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
          colors: [
            Color.fromRGBO(40, 166, 126, 1),
            Color.fromRGBO(92, 230, 159, 1),
          ],
        ),
        borderRadius: BorderRadius.circular(
          Helper.dynamicFont(context, 1),
        ),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          TextWidget(
            textAlign: TextAlign.start,
            text: title,
            color: R.color.whiteColor,
            fontSize: 1.5,
          ),
          SizedBox(
            height: Helper.dynamicHeight(context, 1),
          ),
          TextWidget(
            textAlign: TextAlign.start,
            text: description,
            color: R.color.whiteColor,
            fontSize: 1.1,
            fontFamily: R.fonts.montserratRegular,
            overFlow: TextOverflow.ellipsis,
            maxLines: 2,
          ),
          SizedBox(
            height: Helper.dynamicHeight(context, 2),
          ),
          Row(
            crossAxisAlignment: CrossAxisAlignment.end,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Expanded(
                child: TextWidget(
                  textAlign: TextAlign.start,
                  text: date,
                  color: R.color.whiteColor,
                  fontSize: 1.1,
                  fontFamily: R.fonts.montserratMedium,
                ),
              ),
              Expanded(
                flex: isPaid ? 0 : 2,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    IconButtonWithGradientBackground(
                      onPressed: () => onpressDelete(),
                      icon: Icons.delete,
                    ),
                    Visibility(
                      visible: !isPaid,
                      child: IconButtonWithGradientBackground(
                        onPressed: () => onpressEdit(),
                        icon: Icons.edit,
                      ),
                    ),
                    Visibility(
                      visible: !isPaid,
                      child: IconButtonWithGradientBackground(
                        onPressed: () => onpressPaid(),
                        icon: Icons.paid_sharp,
                      ),
                    ),
                  ],
                ),
              )
            ],
          ),
        ],
      ),
    );
  }
}
