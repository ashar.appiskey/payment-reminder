import 'package:flutter/material.dart';
import 'package:paymentreminder/constants/constants.dart';
import 'package:paymentreminder/services/helper.dart';
import 'package:paymentreminder/widgets/text.dart';

class CustomAppBar extends StatelessWidget with PreferredSizeWidget {
  @override
  final Size preferredSize;
  final String? leadImage;
  final String? title;
  final bool isCenterTitle;
  final bool isLeadingIcon;
  final double elevation;
  final VoidCallback? onTapLeading;
  final bool isTrailingIcon;
  final VoidCallback? onTapTrailing;
  final String? trailingImage;
  final double fontSize;

  CustomAppBar({
    Key? key,
    this.leadImage,
    this.title,
    this.isCenterTitle = true,
    this.isLeadingIcon = true,
    this.elevation = 0.0,
    this.onTapLeading,
    this.isTrailingIcon = false,
    this.onTapTrailing,
    this.trailingImage,
    this.fontSize = 2.5,
  })  : preferredSize = Size.fromHeight(65.0),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return AppBar(
      backgroundColor: R.color.whiteColor,
      automaticallyImplyLeading: isLeadingIcon,
      elevation: elevation,
      title: TextWidget(
        text: title ?? "",
        fontSize: fontSize,
      ),
      centerTitle: isCenterTitle,
      leading: Visibility(
        visible: isLeadingIcon,
        child: GestureDetector(
          onTap: isLeadingIcon ? () => onTapLeading!() : () {},
          child: Center(
            child: Image.asset(
              leadImage ?? "",
              width: Helper.dynamicWidth(context, 5),
              height: Helper.dynamicWidth(context, 5),
            ),
          ),
        ),
      ),
      actions: [
        Visibility(
          visible: isTrailingIcon,
          child: GestureDetector(
            onTap: isTrailingIcon ? () => onTapTrailing!() : () {},
            child: Center(
              child: Image.asset(
                trailingImage ?? "",
                width: Helper.dynamicWidth(context, 5),
                height: Helper.dynamicWidth(context, 5),
              ),
            ),
          ),
        ),
        SizedBox(
          width: Helper.dynamicWidth(context, 2),
        )
      ],
    );
  }
}
