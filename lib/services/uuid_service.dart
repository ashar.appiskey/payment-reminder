import 'package:uuid/uuid.dart';

class UUID {
  static late final Uuid instance;

  UUID._() {
    instance = const Uuid();
  }

  static final UUID _singleton = UUID._();

  static UUID get uuidInstance => _singleton;

  uuid() {
    return instance.v1();
  }

uuid2() {
    return instance.v1obj();
  }

}
