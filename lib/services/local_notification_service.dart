import 'dart:convert';
import 'dart:io';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';


class LocalNotificationService {
  static final FlutterLocalNotificationsPlugin _notificationsPlugin =
      FlutterLocalNotificationsPlugin();

  static void initialize(BuildContext context) async {
    final InitializationSettings initializationSettings =
        InitializationSettings(
      android: AndroidInitializationSettings(
        "@mipmap/ic_launcher",
      ),
      iOS: DarwinInitializationSettings(
        requestAlertPermission: true,
        requestSoundPermission: true,
        requestBadgePermission: true,
        requestCriticalPermission: false,
        defaultPresentAlert: true,
        defaultPresentSound: true,
        defaultPresentBadge: true,
      ),
    );
    await _notificationsPlugin.initialize(initializationSettings);
    _notificationsPlugin.initialize(
      initializationSettings,
      onDidReceiveNotificationResponse: (data) async {
        foregroundNotificationHandler(data.payload!, context);
      },
      // onDidReceiveBackgroundNotificationResponse: (type) async {
      //   // print("$type this is type");
      //   // print('printing type');
      //   backgroundNotificationHandler(type.notificationResponseType, context);
      // },
    );
  }

  static void requestNotificationPermission() {
    //android 13
    _notificationsPlugin
        .resolvePlatformSpecificImplementation<
            AndroidFlutterLocalNotificationsPlugin>()!
        .requestPermission();
  }

  static void display(RemoteMessage message) async {
    try {
      final id = DateTime.now().millisecondsSinceEpoch ~/ 1000;

      final NotificationDetails notificationDetails = NotificationDetails(
        android: AndroidNotificationDetails(
            "FCM_CHANNEL_ID", "FCM_CHANNEL channel",
            channelDescription: "This is dang channel",
            importance: Importance.max,
            priority: Priority.high,
            playSound: true),
      );

      Platform.isAndroid
          ? await _notificationsPlugin.show(
              id,
              message.notification!.title,
              message.notification!.body,
              notificationDetails,
              payload: json.encode(message.data),
            )
          : null;
    } on Exception catch (e) {
      print(e);
    }
  }

  static void foregroundNotificationHandler(
      String message, BuildContext context) async {
    // Map<String, dynamic> parsed = json.decode(message);
    // print("forgroud");
    // Map<String, dynamic> parsed = json.decode(message);
    // print(parsed);

    // Navigator.of(context).pushNamed(
    //   chatScreen,
    //   arguments: {
    //     'user_id': parsed["reciver_id"],
    //   },
    // );
  }

  static void backgroundNotificationHandler(
      RemoteMessage message, BuildContext context) async {
    // print("backgrounnd");
    // print(message.data["reciver_id"]);
    // Navigator.of(context).pushNamed(
    //   chatScreen,
    //   arguments: {
    //     'user_id': message.data["reciver_id"],
    //   },
    // );
  }

  static void terminatedNotificationHandler(
      RemoteMessage message, BuildContext context) async {
    // print("terminated");
    // print(message);
    // print(message.data["reciver_id"]);
    // Navigator.of(context).pushNamed(
    //   chatScreen,
    //   arguments: {
    //     'user_id': message.data["reciver_id"],
    //   },
    // );
  }
}
