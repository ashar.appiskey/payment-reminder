import 'package:flutter/material.dart';

class Helper {
  static dynamicFont(BuildContext context, double fontSize) {
    final mediaQuery = MediaQuery.of(context).size;

    return ((mediaQuery.height + mediaQuery.width) / 100) * fontSize;
  }

  static dynamicWidth(BuildContext context, double width) {
    final mediaQuery = MediaQuery.of(context).size;

    return (mediaQuery.width / 100) * width;
  }

  static dynamicHeight(BuildContext context, double height) {
    final mediaQuery = MediaQuery.of(context).size;

    return (mediaQuery.height / 100) * height;
  }
}
