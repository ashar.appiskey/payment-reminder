// ignore_for_file: non_constant_identifier_names

import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class FirebaseService {
  static late final FirebaseMessaging FMinstance;
  static late final FirebaseAuth FAinstance;
  static late final FirebaseFirestore FFSinstance;

  FirebaseService._();

  firebaseServiceInit() {
    FMinstance = FirebaseMessaging.instance;
    FAinstance = FirebaseAuth.instance;
    FFSinstance = FirebaseFirestore.instance;
  }

  static final FirebaseService _singleton = FirebaseService._();

  static FirebaseService get firebaseInstance => _singleton;
}
