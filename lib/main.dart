import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:paymentreminder/providers/auth_provider.dart';
import 'package:paymentreminder/providers/payments_provider.dart';
import 'package:paymentreminder/providers/toggle_password_field.dart';
import 'package:paymentreminder/routes/index.dart';
import 'package:paymentreminder/routes/router.dart';
import 'package:paymentreminder/services/firebase_service.dart';
import 'package:paymentreminder/services/local_notification_service.dart';
import 'package:paymentreminder/services/shared_preferences.dart';
import 'package:provider/provider.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  await SharedPreferenceManager.init();
  FirebaseService.firebaseInstance.firebaseServiceInit();
  LocalNotificationService.requestNotificationPermission();
  //This code is used for the portrit orientation of the app
  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp])
      .then((_) {
    runApp(MyApp());
  });
}

class MyApp extends StatefulWidget {
  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  String screen = "";

  @override
  void initState() {
    if (SharedPreferenceManager.sharedInstance.hasToken()) {
      screen = homeScreen;
    } else {
      screen = login;
    }
    super.initState();
  }

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider.value(value: TogglePasswordVisibilty()),
        ChangeNotifierProvider.value(value: AuthProvider()),
        ChangeNotifierProvider.value(value: PaymentsProvider())
      ],
      child: MaterialApp(
        title: 'Payment Reminder',
        debugShowCheckedModeBanner: false,
        initialRoute: screen,
        onGenerateRoute: MyRouter().generateRoute,
      ),
    );
  }
}
